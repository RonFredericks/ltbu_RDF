#!/usr/bin/env python3

"""
ltbu.py
Rsync wrapper with some sanity checks to make backups less prone to errors.
"""

import argparse
import datetime
import json
import os
import pathlib
import subprocess
import sys


class LTBUError(Exception):
    """ Generic ltbu exception. """
    pass


def run_backup(destination_str, targets):
    """ Run the backup. """

    try:
        destination = pathlib.Path(destination_str).resolve() / 'Backup'

        print('Backup destination: {}'.format(destination))

        with open(str(destination / 'ltbu.json')) as metafile:
            meta = json.load(metafile)
            print('Backup disk identified itself as: {}'.format(meta['name']))

    except FileNotFoundError:
        raise LTBUError(
            'Could not get metadata. Maybe the destination is not mounted'
            ' or not initialised?'
        )
    except (KeyError, json.decoder.JSONDecodeError):
        raise LTBUError('The backup location seem to be corrupted.')

    try:
        print('Date of last backup: {}'.format(meta['lastbackup']))
    except KeyError:
        print('There was no previous backup.')

    print('Backup destination: {}'.format(destination))

    for target in targets:
        try:
            target_resolved = pathlib.Path(target).resolve()
            dirname = str(target_resolved).replace('/', '_')[1:]
            subdest = destination / dirname

            print('Rsyncing {} to {}'.format(target, subdest))
            process = subprocess.run((
                '/usr/bin/env', 'rsync', '--recursive', '--links', '--times',
                '--delete', '--stats', '--human-readable', '--human-readable',
                str(target_resolved) + '/', str(subdest)
            ), check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            print(process.stdout.decode())
        except (subprocess.CalledProcessError, FileNotFoundError):
            raise LTBUError('The rsync command failed.')

    meta['lastbackup'] = str(datetime.datetime.now())

    with open(str(destination / 'ltbu.json'), 'w') as metafile:
        json.dump(meta, metafile)

    print('\nBackup COMPLETED.')


def init_destination(destination_str, destname):
    """ Initialize backup destination. """
    try:
        destination = pathlib.Path(destination_str).resolve() / 'Backup'
        os.mkdir(str(destination))
        with open(str(destination / 'ltbu.json'), 'x') as metafile:
            json.dump({'name': destname}, metafile)

    except FileNotFoundError:
        raise LTBUError('The destinatios does not exist.')
    except FileExistsError:
        raise LTBUError('The destinations seems to be already initialised.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Long Term Backup with rsync')
    parser.add_argument(
        '-v', '--version', action='version', version='ltbu.py v0.1'
    )
    parser.add_argument(
        '-i', '--init', action='store_true',
        help='initialise new backup destination'
    )
    parser.add_argument('destination', help='destination of the backup')
    args = parser.parse_args()

    if args.init:
        name = input('Specify a name for this backup: ')
        init_destination(args.destination, name)
    else:
        try:
            run_backup(args.destination, ['/data/storage'])
        except LTBUError as e:
            print('Backup FAILED: {}'.format(e))
            sys.exit(1)
