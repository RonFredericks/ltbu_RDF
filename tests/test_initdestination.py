""" Tests for ltbu's init_destination function. """

import json
import os
import pathlib
import shutil
import tempfile
import unittest

import ltbu


class InitDestinationTest(unittest.TestCase):
    def setUp(self):
        self.missing_dir = pathlib.Path('/nonexistent')
        self.dest_dir = pathlib.Path(tempfile.mkdtemp())

    def tearDown(self):
        shutil.rmtree(str(self.dest_dir))

    def test_regular(self):
        ltbu.init_destination(self.dest_dir, 'NewTestBackup')

        backupdir = self.dest_dir / 'Backup'
        metafilepath = backupdir / 'ltbu.json'

        self.assertTrue(backupdir.is_dir())
        self.assertTrue(metafilepath.is_file())

        with open(str(metafilepath)) as metafile:
            metadata = json.load(metafile)

        self.assertEqual(metadata.keys(), {'name'})
        self.assertEqual(metadata['name'], 'NewTestBackup')

        self.assertEqual(set(backupdir.iterdir()), {metafilepath})

    def test_missing_destination(self):
        with self.assertRaises(ltbu.LTBUError):
            ltbu.init_destination(self.missing_dir, 'NewTestBackup')

    def test_destination_exists(self):
        os.mkdir(str(self.dest_dir / 'Backup'))
        shutil.copy(
            'tests/ltbu_working.json',
            str(self.dest_dir / 'Backup' / 'ltbu.json')
        )

        with self.assertRaises(ltbu.LTBUError):
            ltbu.init_destination(self.dest_dir, 'NewTestBackup')
