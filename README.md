# ltbu.py [![build status](https://gitlab.com/mp1409/ltbu/badges/master/build.svg)](https://gitlab.com/mp1409/ltbu/commits/master) #
Rsync wrapper with some sanity checks to make backups less prone to errors.

## Supported python versions ##
* CPython 3.6
* CPython 3.5
* PyPy 3-5.7

## Usage ##
The script can be run by:
```
<python3|pypy3> ltbu.py -h
usage: ltbu.py [-h] [-v] [-i] destination

Long Term Backup with rsync

positional arguments:
  destination    destination of the backup

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
  -i, --init     initialise new backup destination
```

## Unit tests ##
The unit test suite can be run with standard test discovery:
```
<python3|pypy3> -m unittest discover
```